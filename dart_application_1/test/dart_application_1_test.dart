import 'package:dart_application_1/dart_application_1.dart';
import 'package:test/test.dart';
import '../bin/dart_application_1.dart';

void main() {
  group('Exactas', () {
    test('si es juno', () {
      var m = iniciarM("juan");
      var i = iniciarI("juno");
      expect(Exactas(m, i), iniciarM("ju").toSet());
    });

    test('si es juan', () {
      var m = iniciarM("juan");
      var i = iniciarI("juan");
      expect(Exactas(m, i), iniciarM("juan").toSet());
    });
  });

  group('No Exactas', () {
    test('Si es juno', () {
      var m = iniciarM("juan");
      var i = iniciarI("juno");
      List<Prueba> x = [];
      x.add(Prueba(numero: 3, letra: "n"));

      expect(NoExactas(m, i, Exactas(m, i)), x);
    });

    test('Si es Javi', () {
      var m = iniciarM("juan");
      var i = iniciarI("javi");
      List<Prueba> x = [];
      x.add(Prueba(numero: 2, letra: "a"));

      expect(NoExactas(m, i, Exactas(m, i)), x);
    });
  });
}

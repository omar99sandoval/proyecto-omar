import 'package:dart_application_1/dart_application_1.dart'
    as dart_application_1;

void main(List<String> arguments) {
  String m = 'juan';
  String i = 'juno';
/*
  List<Prueba> coinN = [];
  for (var elemento in cip) {
    print('elemento $elemento');
    coinN.addAll(cmp.where((e) {
      print(e);
      return e.letra == elemento.letra && !coin.contains(e);
    }));
  }

  print('Coincidencias exactas');
  print(coin);

  print('Coincidencias no exactas');
  print(coinN);*/

  // faltan las que no coinciden
}

class Prueba {
  final int numero;
  final String letra;

  const Prueba({required this.numero, required this.letra});
  @override
  bool operator ==(other) =>
      other is Prueba && numero == other.numero && letra == other.letra;
  @override
  int get hashCode => numero.hashCode ^ letra.hashCode;
  @override
  toString() => 'P($numero:$letra)';
}

Iterable<Prueba> iniciarM(String m) {
  var ml = m.split('').asMap();

  var cm = ml.entries;
  var cmp = cm.map((e) => Prueba(letra: e.value, numero: e.key));

  return cmp;
}

Iterable<Prueba> iniciarI(String i) {
  var il = i.split('').asMap();

  var ci = il.entries;
  var cip = ci.map((e) => Prueba(letra: e.value, numero: e.key));

  return cip;
}

Set<Prueba> Exactas(Iterable<Prueba> m, Iterable<Prueba> i) {
  return m.toSet().intersection(i.toSet());
}

List<Prueba> NoExactas(
    Iterable<Prueba> m, Iterable<Prueba> i, Set<Prueba> coin) {
  List<Prueba> coinN = [];
  for (var elemento in i) {
    print('elemento $elemento');
    coinN.addAll(m.where((e) {
      print(e);
      return e.letra == elemento.letra && !coin.contains(e);
    }));
  }
  return coinN;
}
